"use strict";

function evaluate(group, alpha, beta, log = true) {

    let min = 0,
        max = 1,
        p = 100,
        a = [],
        b = [],
        w = [],
        c = 0,
        v = 0;

    console.log("[CUSTOMER TYPE: ", group, "]");

    for (var x = min; x <= max; x += 0.1) {
        v = p * Math.pow(x, alpha - 1) * Math.pow((1 - x), (beta - 1));
        a.push(x);
        b.push(v);

        if (log)
            console.log("[", parseFloat((x.toFixed(1))), "]", "TIME:", parseFloat(v.toFixed(2)), "seconds");
    }

    for (var i = 0; i < a.length; i++) {
        if (i == 0)
            c = b[i] + a[i];
        else
            c = c + b[i];

        w[i] = (c - a[i]) - b[i];
    }

    return w;

}

function getMaxAvg(w) {

    const v = (x, y) => Math.max(0, x) + Math.max(0, y);
    const sum = w.reduce(v);

    return [(sum / (w.length - 1)).toFixed(2), Math.max.apply(null, w).toFixed(2)];

}

function answerQ1() {

    console.log("ANSWERING QUESTION NUMBER 1:");
    const w = evaluate("YELLOW", 2, 5);
    const [avg, max] = getMaxAvg(w);

    console.log("ANSWER:");
    console.log("1. AVERAGE CUSTOMER WAITING TIMES:", avg, "seconds");
    console.log("2. MAXIMUM CUSTOMER WAITING TIMES:", max, "seconds");

}

function answerQ2() {

    console.log("ANSWERING QUESTION NUMBER 2:");
    const m = evaluate("RED", 2, 2);

    let g = 0;
    for (var i = 0, len = m.length; i < len; i++)
        g = g + i;

    console.log("ANSWER:", m.length);
    console.log("1. AVERAGE QUEUE LENGTHS IN-FRONT OF THE TELLER:", g / m.length, "persons");
    console.log("2. MAXIMUM QUEUE LENGTHS IN-FRONT OF THE TELLER:", m.indexOf(Math.max.apply(null, m)) - 1, "persons");

}

function answerQ3() {

    let ar = [];

    console.log("ANSWERING QUESTION NUMBER 3:");

    const ym = evaluate("YELLOW", 2, 5, false);
    const [yAvg, yMax] = getMaxAvg(ym);
    ar.push(['YELLOW', yMax - yAvg]);
    console.log(">>AVERAGE CUSTOMER WAITING TIMES:", yAvg, "seconds");
    console.log(">>MAXIMUM CUSTOMER WAITING TIMES:", yMax, "seconds");

    const rm = evaluate("RED", 2, 2, false);
    const [rAvg, rMax] = getMaxAvg(rm);
    ar.push(['RED', rMax - rAvg]);
    console.log(">>AVERAGE CUSTOMER WAITING TIMES:", rAvg, "seconds");
    console.log(">>MAXIMUM CUSTOMER WAITING TIMES:", rMax, "seconds");

    const bm = evaluate("BLUE", 5, 1, false);
    const [bAvg, bMax] = getMaxAvg(bm);
    ar.push(['BLUE', bMax - bAvg]);
    console.log(">>AVERAGE CUSTOMER WAITING TIMES:", bAvg, "seconds");
    console.log(">>MAXIMUM CUSTOMER WAITING TIMES:", bMax, "seconds");

    for (var i = 0, len = ar.length; i < len; i++) {
        if (i == 0)
            var t = ar[i];
        else if (ar[i][1] < t[1])
            t = ar[i];
    }

    console.log("ANSWER:");
    console.log("[", t[0], "] GIVES THE CLOSEST VALUE BETWEEN THE AVERAGE AND MAXIMUM CUSTOMER WAITING TIMES");

}

answerQ1();
console.log("==============================");
answerQ2();
console.log("==============================");
answerQ3();